<?php

namespace Tests\Feature\Post;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */
    public function user_can_delete_post_if_post_exists()
    {
        $post = Post::factory()->create();
        $response = $this->deleteJson(route('posts.destroy', $post->id));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('posts', [
            'id' => $post->id,
            'title' => $post->title,
            'content' => $post->content
        ]);
    }

    /** @test */
    public function user_cant_delete_post_if_post_is_not_exists()
    {
        $postId = -1;
        $response = $this->deleteJson(route('posts.destroy', $postId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
