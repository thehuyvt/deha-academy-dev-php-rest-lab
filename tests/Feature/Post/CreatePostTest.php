<?php

namespace Tests\Feature\Post;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test */
    public function user_create_post_and_data_is_valid()
    {
        $dataCreate = Post::factory()->make()->toArray();
        $response = $this->postJson(route('posts.store'), $dataCreate);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('title', $dataCreate['title'])
                ->where('content', $dataCreate['content'])
                ->etc()
            )->has('message')
        );
        $this->assertDatabaseHas('posts', [
            'title' => $dataCreate['title'],
            'content' => $dataCreate['content']
        ]);
    }

    /** @test */
    public function user_cant_create_post_and_data_is_invalid()
    {
        $dataCreate = [
            'title' => '',
            'content' => ''
        ];

        $response = $this->postJson(route('posts.store'), $dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('error', fn (AssertableJson $json) =>
                $json->has('title')
                ->has('content')
            )
        );
    }

    /** @test */
    public function user_cant_create_post_and_title_is_null()
    {
        $dataCreate = [
            'title' => '',
            'content' => fake()->text,
        ];
        $response = $this->postJson(route('posts.store'), $dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('error', fn (AssertableJson $json) =>
                $json->has('title')
            )
        );
    }

    /** @test */
    public function user_cant_create_post_and_content_is_null()
    {
        $dataCreate = [
            'title' => fake()->title,
            'content' => ''
        ];
        $response = $this->postJson(route('posts.store'), $dataCreate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('error', fn (AssertableJson $json) =>
                $json->has('content')
            )
        );
    }
}
