<?php

namespace Tests\Feature\Post;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */
    public function user_can_update_post_and_data_is_valid()
    {
        $post = Post::factory()->create();
        $postUpdate = Post::factory()->make()->toArray();

        $response = $this->putJson(route('posts.update', $post->id), $postUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('data', fn (AssertableJson $json) =>
                $json->where('title', $postUpdate['title'])
                ->where('content', $postUpdate['content'])
                ->etc()
            )->has('message')
        );
        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'title' => $postUpdate['title'],
            'content' => $postUpdate['content']
        ]);
    }

    /** @test */
    public function user_cant_update_post_and_data_is_invalid()
    {
        $post = Post::factory()->create();
        $postUpdate = [
            'title' => '',
            'content' => ''
        ];
        $response = $this->putJson(route('posts.update', $post->id), $postUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('error', fn (AssertableJson $json) =>
                $json->has('title')
                ->has('content')
            )
        );
    }

    /** @test */
    public function user_cant_update_post_and_title_is_null()
    {
        $post = Post::factory()->create();
        $postUpdate = [
            'title' => '',
            'content' => fake()->text
        ];
        $response = $this->putJson(route('posts.update', $post->id), $postUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('error', fn (AssertableJson $json) =>
                $json->has('title')
                )
        );
    }

    /** @test */
    public function user_cant_update_post_and_content_is_null()
    {
        $post = Post::factory()->create();
        $postUpdate = [
            'title' => fake()->title,
            'content' => ''
        ];
        $response = $this->putJson(route('posts.update', $post->id), $postUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn (AssertableJson $json) =>
            $json->has('error', fn (AssertableJson $json) =>
                $json->has('content')
            )
        );
    }
}
