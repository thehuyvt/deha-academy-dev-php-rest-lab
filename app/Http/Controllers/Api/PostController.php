<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Response;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = Post::query()->paginate(5);

        $data = new PostCollection($posts);

        return $this->sentSuccessResponse($data, 'get list posts successfully', Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request)
    {
        $dataRequest = $request->validated();
        $post = Post::query()->create($dataRequest);

        $postResource = new PostResource($post);
        return $this->sentSuccessResponse($postResource, 'create post successfully', Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = Post::query()->findOrFail($id);

        $postResource = new PostResource($post);
        return $this->sentSuccessResponse($postResource, 'get post successfully', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePostRequest $request, string $id)
    {
        $post = Post::query()->findOrFail($id);
        $dataRequest = $request->all();
        $post->update($dataRequest);
        $postResource = new PostResource($post);
        return $this->sentSuccessResponse($postResource, 'update post successfully', Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = Post::query()->findOrFail($id);
        $post->delete();
        $postResource = new PostResource($post);
        return $this->sentSuccessResponse($postResource, 'delete post successfully', Response::HTTP_OK);
    }
}
